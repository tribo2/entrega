package br.com.solutis;
import robocode.*;
import java.awt.Color;
import static robocode.util.Utils.normalRelativeAngleDegrees;
//import java.awt.Color;

// API help : https://robocode.sourceforge.io/docs/robocode/robocode/Robot.html

/**
 * RoboTriboVamo - a robot by (your name here)
 */
public class RoboTriboVamo extends AdvancedRobot
{
int gunDirection = 1;
	int moveDirection = 1;
	int dist = 50;
	/**
	 * run: RoboDoArthur's default behavior
	 */
	public void run() {
	
		setColors(Color.white, Color.black, Color.white);

		while(true) {
			turnGunRight(20);
		}
	}

	public void onScannedRobot(ScannedRobotEvent e) {
			
		if (e.getDistance() < 50 && getEnergy() > 30) {
			fire(5);
		}
		else {
			fire(1);
		}
	
		scan();			

	}


	public void onHitByBullet(HitByBulletEvent e) {
	    turnRight(normalRelativeAngleDegrees(90 - (getHeading() - e.getHeading())));

		ahead(dist);
		dist *= -1;
		scan();
	}
	

	public void onHitWall(HitWallEvent e) {
		moveDirection=-moveDirection;
	}	
}
